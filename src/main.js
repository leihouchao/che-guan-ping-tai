import Vue from 'vue'
import App from './App'
import router from './router'
import $ from 'jquery'
import iView from 'iView'
import VueResource from 'vue-resource';
import 'lib-flexible'
require('iView/dist/styles/iview.css')

Vue.use(VueResource);
Vue.use(iView);
Vue.config.productionTip = false

// 初始化leanCloud
var APP_ID = 'zUfNzhe6IcXu9UL7s041DNkq-gzGzoHsz'
var APP_KEY = 'el3SgafAHPURilc5sYXVIjqu'

AV.init({
    appId: APP_ID,
    appKey: APP_KEY
});

new Vue({
     el: '#app',
     router,
     template: '<App/>',
     components: {App}
});
