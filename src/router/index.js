import Vue from 'vue'
import Router from 'vue-router'

// 登陆
const Login =  r => require.ensure([], () => r(require('../pages/Login')), 'a')
const Main =  r => require.ensure([], () => r(require('../pages/main')), 'b')
const WantAccess =  r => require.ensure([], () => r(require('../pages/wantAccess')), 'c')

// 电子围栏
const travelFance =  r => require.ensure([], () => r(require('../pages/travelFance')), 'f')
const rentalFance =  r => require.ensure([], () => r(require('../pages/rentalFance')), 'g')

// 权限管理
const addadmin =  r => require.ensure([], () => r(require('../pages/addadmin')), 'i')
const addbusiness =  r => require.ensure([], () => r(require('../pages/addbusiness')), 'j')
const addproduct =  r => require.ensure([], () => r(require('../pages/addproduct')), 'jk')
const addoperation =  r => require.ensure([], () => r(require('../pages/addoperation')), 'yy')
const addFranchiser =  r => require.ensure([], () => r(require('../pages/addFranchiser')), 'l')

// 系统配置
const faultcfg =  r => require.ensure([], () => r(require('../pages/faultcfg')), 'yyt')
const computeCost =  r => require.ensure([], () => r(require('../pages/computeCost')), 'p')
const telConfig =  r => require.ensure([], () => r(require('../pages/configTel')), 'q')
const helpPageConfig =  r => require.ensure([], () => r(require('../pages/configHelpPage')), 's')

// 车辆录入
const uploadFile =  r => require.ensure([], () => r(require('../pages/uploadFile')), 'n')
const batchUpload =  r => require.ensure([], () => r(require('../pages/batchUpload')), 'no')
const downLoadFile =  r => require.ensure([], () => r(require('../pages/downLoadFile')), 'w')

/************** 2018/01/02 改版******************************************/
// 车辆管理
const newCarManager =  r => require.ensure([], () => r(require('../pages/newCarManager')), 't')
const newCarManagerInfo =  r => require.ensure([], () => r(require('../pages/newCarManagerInfo')), 'u')
// 用户工单
const newUserList =  r => require.ensure([], () => r(require('../pages/newUserList')), 'j')
const newUserListInfo =  r => require.ensure([], () => r(require('../pages/newUserListInfo')), 'k')
// 加盟商管理
const newCarAllocate =  r => require.ensure([], () => r(require('../pages/newCarAllocate')), 'm')
const FranchiserBoard =  r => require.ensure([], () => r(require('../pages/FranchiserBoard')), 'n')
const FranchiserCars =  r => require.ensure([], () => r(require('../pages/FranchiserCars')), 'v')
// 加盟商车辆列表
const orderList =  r => require.ensure([], () => r(require('../pages/orderList')), 'x')
const orderInfo =  r => require.ensure([], () => r(require('../pages/orderInfo')), 'y')

/************************************************************************/
Vue.use(Router)

export default new Router({
      routes: [
        // 没有权限
        {
           path:"/WantAccess",
           name:"WantAccess",
           component:WantAccess,
        },
        //主界面
        {
          path:"/Main",
          name:"Main",
          component:Main,
          children:[
            /***********************************************/
             // 新的 订单列表 页面
             {
              path:"/orderList",
              name:"orderList",
              component:orderList,
            },
            // 新的 车辆总表 页面
            {
              path:"/newCarManager",
              name:"newCarManager",
              component:newCarManager,
            },
            // 新的 用户列表 页面
            {
              path:"/newUserList",
              name:"newUserList",
              component:newUserList,
            },
            // 车辆信息 导出界面
            {
                path:"/downLoadFile",
                name:"downLoadFile",
                component:downLoadFile,
            },
            // 加盟商看板
            {
              path:"/FranchiserBoard",
              name:"FranchiserBoard",
              component:FranchiserBoard,
            },
            // 添加加盟商
            {
                path:"/addFranchiser",
                name:"addFranchiser",
                component:addFranchiser,
            },
            // 加盟商车辆分配
            {
                path:"/newCarAllocate",
                name:"newCarAllocate",
                component:newCarAllocate,
            },
            // 加盟商 车辆列表
            {
              path:"/FranchiserCars",
              name:"FranchiserCars",
              component:FranchiserCars,
            },
            /***********************************************/
            // 租还围栏 
            {
              path:"/rentalFance",
              name:"rentalFance",
              component:rentalFance,
             },
            // 可行驶围栏 
            {
              path:"/travelFance",
              name:"travelFance",
              component:travelFance,
             },
            // 添加管理员
            {
              path:"/addadmin",
              name:"addadmin",
              component:addadmin,
             },
            // 添加运营
            {
              path:"/addbusiness",
              name:"addbusiness",
              component:addbusiness,
             },
            // 添加生产
            {
              path:"/addproduct",
              name:"addproduct",
              component:addproduct,
             },
            // 添加运维
            {
              path:"/addoperation",
              name:"addoperation",
              component:addoperation,
             },
            // 故障类型
            {
              path:"/faultcfg",
              name:"faultcfg",
              component:faultcfg,
             },
            // 上传文件
            {
              path:"/uploadFile",
              name:"uploadFile",
              component:uploadFile,
             },
            // 在线上传
             {
              path:"/batchUpload",
              name:"batchUpload",
              component:batchUpload,
             },
            // 计费配置
            {
              path:"/computeCost",
              name:"computeCost",
              component:computeCost,
             },
            // 客服电话配置
            {
              path:"/telConfig",
              name:"telConfig",
              component:telConfig,
             },
            // 帮助页配置
            {
              path:"/helpPageConfig",
              name:"helpPageConfig",
              component:helpPageConfig,
             },

          ]
        },
        //  登录页面
        {
          path:"/",
          name:"Login",
          component:Login,
        },
        /***********************************************/
        // 新的 工单详情 页面
        {
          path:"/newCarManagerInfo",
          name:"newCarManagerInfo",
          component:newCarManagerInfo,
        },
        // 新的 用户详情 页面
        {
            path:"/newUserListInfo",
            name:"newUserListInfo",
            component:newUserListInfo,
        },
        // 新的 订单详情 页面
        {
          path:"/orderInfo",
          name:"orderInfo",
          component:orderInfo,
        },
        /***********************************************/
     ]
})

